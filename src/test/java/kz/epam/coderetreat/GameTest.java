package kz.epam.coderetreat;

import kz.epam.coderetreat.cell.AliveCell;
import kz.epam.coderetreat.cell.DeadCell;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    public static final int WIDTH = 5;
    public static final int HEIGHT = 3;
    @Mock
    private Board board;

    @Mock
    private Printer printer;

    private Game game;

    @Before
    public void setUp() {
        game = new Game(board, printer);

        when(board.getWidth()).thenReturn(WIDTH);
        when(board.getHeight()).thenReturn(HEIGHT);
    }

    @Test
    public void allDead_5x3Board_PrintsThreeRows_Of5Minuses() {
        when(board.getCell(any(Board.Point.class))).thenReturn(new DeadCell());

        game.print();

        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);

        verify(printer, times(3)).println(stringCaptor.capture());

        assertEquals("-----", stringCaptor.getAllValues().get(0));
        assertEquals("-----", stringCaptor.getAllValues().get(1));
        assertEquals("-----", stringCaptor.getAllValues().get(2));
    }

    @Test
    public void allDead_5x3Board_PrintsThreeRows_Of5Pluses() {
        when(board.getCell(any(Board.Point.class))).thenReturn(new AliveCell());

        game.print();

        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);

        verify(printer, times(3)).println(stringCaptor.capture());

        assertEquals("+++++", stringCaptor.getAllValues().get(0));
        assertEquals("+++++", stringCaptor.getAllValues().get(1));
        assertEquals("+++++", stringCaptor.getAllValues().get(2));
    }

    @Test
    public void firstDeadSecondAliveRow_2x3Board_PrintsPlusOnTop_AndMinusAtBottom() {
        when(board.createPoint(anyInt(), anyInt())).thenCallRealMethod();
        mockRow1WithDeadCells();
        mockRow2WithAliveCells();
        mockRow3WithOscillatingCells();

        game.print();

        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);

        verify(printer, times(3)).println(stringCaptor.capture());

        assertEquals("+-+-+", stringCaptor.getAllValues().get(0));
        assertEquals("+++++", stringCaptor.getAllValues().get(1));
        assertEquals("-----", stringCaptor.getAllValues().get(2));
    }

    private void mockRow1WithDeadCells() {
        for (int x = 1; x <= WIDTH; x++) {
            Board.Point point = board.createPoint(x, 1);
            when(board.getCell(point)).thenReturn(new DeadCell());
        }
    }

    private void mockRow2WithAliveCells() {
        for (int x = 1; x <= WIDTH; x++) {
            Board.Point point = board.createPoint(x, 2);
            when(board.getCell(point)).thenReturn(new AliveCell());
        }
    }

    private void mockRow3WithOscillatingCells() {
        for (int x = 1; x <= WIDTH; x++) {
            Board.Point point = board.createPoint(x, 3);
            boolean isAlive = x % 2 == 1;
            when(board.getCell(point)).thenReturn(isAlive ? new AliveCell() : new DeadCell());
        }
    }
}