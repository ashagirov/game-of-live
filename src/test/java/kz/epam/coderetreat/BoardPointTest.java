package kz.epam.coderetreat;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardPointTest {

    public static final int POINT_X = 3;
    public static final int POINT_Y = 4;
    public static final int BOARD_WIDTH = 5;
    public static final int BOARD_HEIGHT = 5;
    private Board board;

    @Before
    public void setUp() throws Exception {
        board = new Board(BOARD_WIDTH, BOARD_HEIGHT);
    }

    @Test
    public void boardPointFactory_ForCoordinatesWithingBoard_AllowsToCreateGivenPoint() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        assertEquals(POINT_X, point.getX());
        assertEquals(POINT_Y, point.getY());
    }

    @Test
    public void boardPointFactory_ForCoordinatesLessThanBoundary_WillProduceOne() {
        Board.Point xLower = board.createPoint(0, POINT_Y);
        Board.Point yLower = board.createPoint(POINT_X, 0);
        Board.Point bothLower = board.createPoint(-1, -1);

        assertEquals(1, xLower.getX());
        assertEquals(1, yLower.getY());

        assertEquals(1, bothLower.getX());
        assertEquals(1, bothLower.getY());
    }

    @Test
    public void boardPointFactory_ForCoordinatesExceedingBoard_ProducesMaxValue() {
        Board.Point xHigher = board.createPoint(BOARD_WIDTH + 1, POINT_Y);
        Board.Point yHigher = board.createPoint(POINT_X, BOARD_HEIGHT + 10);
        Board.Point bothHigher = board.createPoint(BOARD_WIDTH + 12, BOARD_HEIGHT + 1);

        assertEquals(BOARD_WIDTH, xHigher.getX());
        assertEquals(BOARD_HEIGHT, yHigher.getY());

        assertEquals(BOARD_WIDTH, bothHigher.getX());
        assertEquals(BOARD_HEIGHT, bothHigher.getY());
    }

    @Test
    public void boardPoint_Above_SameX_AndYplusOne() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        Board.Point above = point.getAbove();
        assertEquals(POINT_X, above.getX());
        assertEquals(POINT_Y + 1, above.getY());
    }

    @Test
    public void boardPoint_Right_XplusOne_SameY() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        Board.Point righ = point.getRight();
        assertEquals(POINT_X + 1, righ.getX());
        assertEquals(POINT_Y, righ.getY());
    }

    @Test
    public void boardPoint_Below_SameX_AndYminusOne() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        Board.Point below = point.getBelow();
        assertEquals(POINT_X, below.getX());
        assertEquals(POINT_Y - 1, below.getY());
    }

    @Test
    public void boardPoint_Left_XminusOne_SameY() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        Board.Point left = point.getLeft();
        assertEquals(POINT_X - 1, left.getX());
        assertEquals(POINT_Y, left.getY());
    }

}