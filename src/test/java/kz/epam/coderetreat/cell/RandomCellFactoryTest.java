package kz.epam.coderetreat.cell;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest(RandomCellFactory.class)
public class RandomCellFactoryTest {

    @InjectMocks
    private RandomCellFactory cellFactory = new RandomCellFactory();

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Math.class);
    }

    @Test
    public void ifRandomLessThanHalf_ReturnsAliveCell() {
        when(Math.random()).thenReturn(0.3d);

        Cell cell = cellFactory.createCell(0, 0);

        assertTrue(cell instanceof AliveCell);
    }

    @Test
    public void ifRandomMoreThanHalf_ReturnsDeadCell() {
        when(Math.random()).thenReturn(0.7d);

        Cell cell = cellFactory.createCell(0, 0);

        assertTrue(cell instanceof DeadCell);
    }

}