package kz.epam.coderetreat;

import kz.epam.coderetreat.cell.AliveCell;
import kz.epam.coderetreat.cell.Cell;
import kz.epam.coderetreat.cell.CellFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class BoardTest {

    public static final int WIDTH = 5;
    public static final int HEIGHT = 6;
    public static final int CELL_X_COORDINATE = 2;
    public static final int CELL_Y_COORDINATE = 3;

    private Board board;

    @Before
    public void setUp() {
        this.board = new Board(WIDTH, HEIGHT);
    }

    @Test
    public void when_YouCreateGameWithDimensions_YouCanAccessThemWithGetters() {
        assertEquals(WIDTH, board.getWidth());
        assertEquals(HEIGHT, board.getHeight());
    }

    @Test
    public void when_YouCreateDefaultGame_YouCanAccessIndividualCells() {
        Board.Point point = board.createPoint(CELL_X_COORDINATE, CELL_Y_COORDINATE);

        Cell cell = board.getCell(point);

        assertNotNull(cell);
    }

    @Test
    public void when_YouSelectDifferentCoordinates_CellsAreNotSame() {
        Board.Point point_1 = board.createPoint(CELL_X_COORDINATE, CELL_Y_COORDINATE);
        Board.Point point_2 = board.createPoint(CELL_X_COORDINATE - 1, CELL_Y_COORDINATE - 1);

        Cell cell_1 = board.getCell(point_1);
        Cell cell_2 = board.getCell(point_2);

        assertNotSame(cell_1, cell_2);
    }

    @Test
    public void when_YouProvideCustomFactory_ItUsesItToInitializeTheBoard() {
        CellFactory cellFactoryMock = mock(CellFactory.class);
        when(cellFactoryMock.createCell(anyInt(), anyInt())).thenReturn(new AliveCell());

        board = new Board(WIDTH, HEIGHT, cellFactoryMock);

        verify(cellFactoryMock, times(WIDTH * HEIGHT)).createCell(anyInt(), anyInt());
    }

    @Test
    public void when_GenerateNewGenerationBoard_ItInvokesNextGenerationOnEveryCell() {
        Cell cellMock = mock(Cell.class);
        CellFactory cellFactoryMock = mock(CellFactory.class);

        when(cellFactoryMock.createCell(anyInt(), anyInt())).thenReturn(cellMock);
        when(cellMock.nextGeneration(anyList())).thenReturn(new AliveCell());

        board = new Board(WIDTH, HEIGHT, cellFactoryMock);
        board.nextGeneration();

        verify(cellMock, times(WIDTH * HEIGHT)).nextGeneration(anyList());
    }

    @Test
    public void when_GenerateNewGenerationBoard_NewBoardIsFilledWith_NextGenCells() {
        Cell cellMock = mock(Cell.class);
        CellFactory cellFactoryMock = mock(CellFactory.class);

        when(cellFactoryMock.createCell(anyInt(), anyInt())).thenReturn(cellMock);
        when(cellMock.nextGeneration(anyList())).thenReturn(new AliveCell());

        board = new Board(WIDTH, HEIGHT, cellFactoryMock);
        Board nextGenBoard = board.nextGeneration();

        for (int x = 0; x < nextGenBoard.getWidth(); x++) {
            for (int y = 0; y < nextGenBoard.getHeight(); y++) {
                Cell cell = nextGenBoard.getCell(nextGenBoard.createPoint(x, y));
                assertTrue(cell.isAlive());
            }
        }
    }

}