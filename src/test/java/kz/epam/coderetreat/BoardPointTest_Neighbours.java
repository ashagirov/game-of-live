package kz.epam.coderetreat;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class BoardPointTest_Neighbours {

    public static final int POINT_X = 3;
    public static final int POINT_Y = 4;
    public static final int BOARD_WIDTH = 5;
    public static final int BOARD_HEIGHT = 5;

    private static final int FIRST_CELL_X_OR_Y = 1;

    private Board board;

    @Before
    public void setUp() throws Exception {
        board = new Board(BOARD_WIDTH, BOARD_HEIGHT);
    }

    @Test
    public void boardPoint_GetNeighbours_ReturnsListOfNeighbourPoints() {
        Board.Point point = board.createPoint(POINT_X, POINT_Y);

        List<Board.Point> neighbours = point.getNeighbours();

        assertThat(neighbours, CoreMatchers.hasItems(
                board.createPoint(POINT_X - 1, POINT_Y - 1),
                board.createPoint(POINT_X - 1, POINT_Y),
                board.createPoint(POINT_X - 1, POINT_Y + 1),

                board.createPoint(POINT_X, POINT_Y - 1),
                board.createPoint(POINT_X, POINT_Y + 1),

                board.createPoint(POINT_X + 1, POINT_Y - 1),
                board.createPoint(POINT_X + 1, POINT_Y),
                board.createPoint(POINT_X + 1, POINT_Y + 1)
        ));
    }

    @Test
    public void cornerCells_ShouldReturnThreeNeighbours() {
        assertEquals(3, board.createPoint(FIRST_CELL_X_OR_Y, FIRST_CELL_X_OR_Y).getNeighbours().size());
        assertEquals(3, board.createPoint(BOARD_WIDTH, FIRST_CELL_X_OR_Y).getNeighbours().size());
        assertEquals(3, board.createPoint(FIRST_CELL_X_OR_Y, BOARD_HEIGHT).getNeighbours().size());
        assertEquals(3, board.createPoint(BOARD_WIDTH, BOARD_HEIGHT).getNeighbours().size());
    }

    @Test
    public void cornerCells_CheckBottomRightCell() {
        List<Board.Point> neighbours = board.createPoint(BOARD_WIDTH, FIRST_CELL_X_OR_Y).getNeighbours();

        assertThat(neighbours, CoreMatchers.hasItems(
                board.createPoint(BOARD_WIDTH - 1, FIRST_CELL_X_OR_Y),
                board.createPoint(BOARD_WIDTH - 1, FIRST_CELL_X_OR_Y + 1),
                board.createPoint(BOARD_WIDTH, FIRST_CELL_X_OR_Y + 1)
        ));
    }

    @Test
    public void cornerCells_CheckTopLeftCell() {
        List<Board.Point> neighbours = board.createPoint(FIRST_CELL_X_OR_Y, BOARD_HEIGHT).getNeighbours();

        assertThat(neighbours, CoreMatchers.hasItems(
                board.createPoint(FIRST_CELL_X_OR_Y + 1, BOARD_HEIGHT),
                board.createPoint(FIRST_CELL_X_OR_Y + 1, BOARD_HEIGHT - 1),
                board.createPoint(FIRST_CELL_X_OR_Y, BOARD_HEIGHT - 1)
        ));
    }

}