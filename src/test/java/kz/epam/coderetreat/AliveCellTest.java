package kz.epam.coderetreat;

import kz.epam.coderetreat.cell.AliveCell;
import kz.epam.coderetreat.cell.Cell;
import kz.epam.coderetreat.cell.DeadCell;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AliveCellTest {

    public static final int MAX_NEIGHBOUR_COUNT = 4;
    private Cell alive = new AliveCell();

    @Test
    public void alive_With_No_AliveNeighbours_StaysDead() {
        List<Cell> neighbours = aliveNeighbourGenerator(0);

        assertFalse(alive.nextGeneration(neighbours).isAlive());
    }

    @Test
    public void alive_With_One_AliveNeighbours_StaysDead() {
        List<Cell> neighbours = aliveNeighbourGenerator(1);

        assertFalse(alive.nextGeneration(neighbours).isAlive());
    }

    @Test
    public void alive_With_Two_AliveNeighbours_StaysDead() {
        List<Cell> neighbours = aliveNeighbourGenerator(2);

        assertTrue(alive.nextGeneration(neighbours).isAlive());
    }

    @Test
    public void alive_With_Three_AliveNeighbours_StaysDead() {
        List<Cell> neighbours = aliveNeighbourGenerator(3);

        assertTrue(alive.nextGeneration(neighbours).isAlive());
    }

    @Test
    public void alive_With_All_AliveNeighbours_StaysDead() {
        List<Cell> neighbours = aliveNeighbourGenerator(4);

        assertFalse(alive.nextGeneration(neighbours).isAlive());
    }

    @Test
    public void LOL_test_testAliveGenerator() {
        assertEquals(4, aliveNeighbourGenerator(0).size());
        assertEquals(4, aliveNeighbourGenerator(1).size());
        assertEquals(4, aliveNeighbourGenerator(2).size());
        assertEquals(4, aliveNeighbourGenerator(3).size());
    }

    private List<Cell> aliveNeighbourGenerator(int aliveNeighbourCount) {
        List<Cell> result = new ArrayList<>();

        for (int i = 0; i < aliveNeighbourCount; i++) {
            result.add(new AliveCell());
        }

        for (int i = 0; i < MAX_NEIGHBOUR_COUNT - aliveNeighbourCount; i++) {
            result.add(new DeadCell());
        }

        return result;
    }

}