package kz.epam.coderetreat;

import kz.epam.coderetreat.cell.Cell;
import kz.epam.coderetreat.cell.CellFactory;
import kz.epam.coderetreat.cell.RandomCellFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Board {

    private Cell[][] grid;
    private CellFactory cellFactory;

    public Board(int width, int height, CellFactory cellFactory) {
        this.cellFactory = cellFactory;
        initBoard(width, height);
    }

    public Board(int width, int height) {
        cellFactory = new RandomCellFactory();
        initBoard(width, height);
    }

    private Board(Cell[][] grid) {
        this.grid = grid;
    }

    private void initBoard(int width, int height) {
        grid = new Cell[width][height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                grid[x][y] = cellFactory.createCell(x, y);
            }
        }
    }

    public int getWidth() {
        return grid.length;
    }

    public int getHeight() {
        return grid[0].length;
    }

    public Cell getCell(Point point) {
        if (point.isOutsideBoundaries()) {
            // Always red in coverage as it is impossible to get here
            throw new IllegalStateException("Point is outside grid boundaries");
        }
        int arrayX = point.getX() - 1;
        int arrayY = point.getY() - 1;
        return grid[arrayX][arrayY];
    }

    public Point createPoint(int x, int y) {
        if (x < 1) x = 1;
        if (y < 1) y = 1;

        if (x > getWidth()) x = getWidth();
        if (y > getHeight()) y = getHeight();

        return new Point(x, y);
    }

    public Board nextGeneration() {
        Cell[][] nextGenerationGrid = new Cell[getWidth()][getHeight()];
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                Point point = createPoint(x + 1, y + 1);
                List<Cell> neighbours = getCells(point.getNeighbours());
                nextGenerationGrid[x][y] = getCell(point).nextGeneration(neighbours);
            }
        }
        return new Board(nextGenerationGrid);
    }

    private List<Cell> getCells(List<Point> points) {
        return points.stream().map(this::getCell).collect(Collectors.toList());
    }

    /**
     * Utility coordinate class. Starts from 1 it is easier to follow board abstraction.
     */
    public class Point {
        private int x, y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Point getAbove() {
            return new Point(x, y + 1);
        }

        public Point getRight() {
            return new Point(x + 1, y);
        }

        public Point getBelow() {
            return new Point(x, y - 1);
        }

        public Point getLeft() {
            return new Point(x - 1, y);
        }

        private boolean isOutsideBoundaries() {
            return getX() < 1 || getX() > getWidth() || getY() < 1 || getY() > getHeight();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x &&
                    y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        public List<Point> getNeighbours() {
            Set<Point> points = new HashSet<>();

            points.add(createPoint(x - 1, y - 1));
            points.add(createPoint(x - 1, y));
            points.add(createPoint(x - 1, y + 1));

            points.add(createPoint(x, y - 1));
            points.add(createPoint(x, y + 1));

            points.add(createPoint(x + 1, y - 1));
            points.add(createPoint(x + 1, y));
            points.add(createPoint(x + 1, y + 1));

            points.remove(this); // in case factory guards created same coordinate

            return new ArrayList<>(points);
        }
    }

}
