package kz.epam.coderetreat;

public interface Printer {

    void println(String row);
}
