package kz.epam.coderetreat.cell;

public class RandomCellFactory implements CellFactory {

    @Override
    public Cell createCell(int x, int y) {
        if (Math.random() < 0.5) {
            return new AliveCell();
        } else {
            return new DeadCell();
        }
    }
}
