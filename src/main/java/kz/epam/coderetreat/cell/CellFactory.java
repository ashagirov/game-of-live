package kz.epam.coderetreat.cell;

public interface CellFactory {

    Cell createCell(int x, int y);

}
