package kz.epam.coderetreat.cell;

import java.util.List;

public interface Cell {

    boolean isAlive();

    Cell nextGeneration(List<Cell> neighbours);
}
