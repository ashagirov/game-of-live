package kz.epam.coderetreat.cell;

import java.util.List;

public class DeadCell implements Cell {

    public boolean isAlive() {
        return false;
    }

    public Cell nextGeneration(List<Cell> neighbours) {
        long aliveNeighBourCount = neighbours.stream().filter(Cell::isAlive).count();
        if (aliveNeighBourCount == 3) {
            return new AliveCell();
        }
        return new DeadCell();
    }

}
