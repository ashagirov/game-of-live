package kz.epam.coderetreat.cell;

import java.util.List;

public class AliveCell implements Cell {

    public boolean isAlive() {
        return true;
    }

    public Cell nextGeneration(List<Cell> neighbours) {
        long aliveNeighbourCount = neighbours.stream().filter(Cell::isAlive).count();

        if (aliveNeighbourCount == 2 || aliveNeighbourCount == 3) {
            return new AliveCell();
        }

        return new DeadCell();
    }

}
