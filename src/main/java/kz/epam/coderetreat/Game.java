package kz.epam.coderetreat;

public class Game {

    private Board board;
    private final Printer printer;

    public Game(Board board, Printer printer) {
        this.board = board;
        this.printer = printer;
    }

    public void print() {
        for (int y = board.getHeight(); y > 0; y--) {
            StringBuilder row = new StringBuilder();
            for (int x = 1; x <= board.getWidth(); x++) {
                row.append(board.getCell(board.createPoint(x, y)).isAlive() ? "+" : "-");
            }
            printer.println(row.toString());
        }
    }

    public void nextGen() {
        this.board = this.board.nextGeneration();
    }

    public static void main(String[] args) {
        Game game = new Game(new Board(10, 5), new SysOutPrinter());
        game.print();

        System.out.println("");
        game.nextGen();
        game.print();

        System.out.println("");
        game.nextGen();
        game.print();
    }

}
